package main

type ErrorResponse struct {
	Error string `json:"error"`
}

type Versions struct {
	Version   string `json:"version"`
	Commit    string `json:"commit"`
	BuildTime string `json:"buildTime"`
}

type Author struct {
	Name      string `json:"name"`
	AuthorKey string `json:"authorKey"`
}

type AuthorKeys struct {
	Authors []AuthorKey `json:"authors"`
}

type AuthorKey struct {
	Key string `json:"key"`
}

type EndpointWorksResponse struct {
	Name        string `json:"name"`
	Revision    int    `json:"revision"`
	PublishDate string `json:"publishDate"`
}

type Work struct {
	Title    string `json:"title"`
	Revision int    `json:"revision"`
	Created  struct {
		Value string `json:"value"`
	}
}

type WorksResponse struct {
	Entries []Work `json:"entries"`
}
