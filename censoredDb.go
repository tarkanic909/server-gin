package main

import (
	"sync"
)

type CensoredDb struct {
	mux             sync.RWMutex
	censoredAuthors map[string]string
}

func (cdb *CensoredDb) GetInstace(instance *CensoredDb) *CensoredDb {
	if instance == nil {
		return &CensoredDb{censoredAuthors: make(map[string]string, 0)}
	}
	return instance

}

func (cdb *CensoredDb) saveCensoredAuthors(authors []string) {
	cdb.mux.Lock()
	for _, item := range authors {
		cdb.censoredAuthors[item] = item
	}
	cdb.mux.Unlock()

}

func (cdb *CensoredDb) checkCensoredAuthors(author string) bool {
	cdb.mux.RLock()
	for _, item := range cdb.censoredAuthors {
		if author == item {
			return true
		}

	}
	cdb.mux.RUnlock()
	return false
}

func (cdb *CensoredDb) getCensoredAuthors() []string {
	cenArr := make([]string, len(cdb.censoredAuthors))
	cdb.mux.RLock()
	i := 0
	for _, item := range cdb.censoredAuthors {
		cenArr[i] = item
		i++
	}
	cdb.mux.RUnlock()
	return cenArr
}
