package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/spf13/viper"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware
	_ "github.com/tarkanic909/server-gin/docs"
)

var version string
var commit string
var buildTime string
var censoredDb *CensoredDb
var openLibraryUrl string
var config Config
var validate validator.Validate

var (
	httpRquestCounter = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "http_requests_total",
		Help: "Counting all requests to srver",
	},
		[]string{"code", "url"},
	)
)

func handler() gin.HandlerFunc {
	return func(c *gin.Context) {

		path := c.Request.URL.Path
		c.Next()
		status := fmt.Sprintf("%d", c.Writer.Status())
		// if _, err := httpRquestCounter.GetMetricWithLabelValues(status, path); err != nil {
		// 	fmt.Print(err)
		// }
		counter, err := httpRquestCounter.GetMetricWith(map[string]string{"url": path, "code": status})
		if err != nil {
			fmt.Print(err)
		}
		counter.Inc()
	}
}

func getHello(c *gin.Context) {
	c.String(http.StatusOK, "Hello %s %s", "Hello", "from gin")
}

// @Summary      Get books array
// @Description  Responds with the list of all books as JSON.
// @Produce      json
// @Success      200
// @Router       /version [get]
func enpGetVersion(c *gin.Context) {
	// time.Sleep(20 * time.Second)
	var res = Versions{version, commit, buildTime}
	c.JSON(http.StatusOK, res)

}

func getAuthorsByBookId(c *gin.Context) {
	bookId := c.Query("book")
	if bookId == "" {
		c.Writer.WriteHeader(http.StatusBadRequest)
		return
	}

	var authorKeys AuthorKeys
	var authorArr []Author

	err := getAuthorsKey(bookId, &authorKeys)
	if err != nil {
		c.JSON(http.StatusBadRequest, ErrorResponse{err.Error()})
		return
	}

	for _, item := range authorKeys.Authors {
		var author Author
		getAuthorByKey(item.Key, &author)
		authorArr = append(authorArr, Author{author.Name, strings.Trim(item.Key, "/authors/")})
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, ErrorResponse{err.Error()})
		return
	}

	c.JSON(200, authorArr)
}

func getAuthorsKey(bookId string, authorKeys *AuthorKeys) error {
	res, err := http.Get(openLibraryUrl + "/isbn/" + bookId + ".json")
	if err != nil {
		return err
	}
	defer res.Body.Close()
	body, _ := io.ReadAll(res.Body)
	json.Unmarshal(body, authorKeys)
	return nil
}

func getAuthorByKey(key string, authors *Author) error {
	res, err := http.Get(openLibraryUrl + key + ".json")
	if err != nil {
		return err
	}
	defer res.Body.Close()
	body, _ := io.ReadAll(res.Body)
	json.Unmarshal(body, authors)
	return nil

}

func enpGetBooksByAuthor(c *gin.Context) {

	authorId := c.Query("author")
	if authorId == "" {
		// c.Writer.WriteHeader(http.StatusBadRequest)
		c.Status(http.StatusBadRequest)
		return
	}

	works, err := getAuthorsWorks(authorId)
	if err != nil {
		c.JSON(http.StatusInternalServerError, ErrorResponse{Error: err.Error()})
		return
	}

	var worksRes []EndpointWorksResponse
	for _, item := range works {
		worksRes = append(worksRes, EndpointWorksResponse{Name: item.Title, Revision: item.Revision, PublishDate: item.Created.Value})

	}

	c.JSON(http.StatusOK, worksRes)

}

func getAuthorsWorks(authorOl string) ([]Work, error) {
	if censoredDb.checkCensoredAuthors(authorOl) {
		return nil, errors.New("Author censored")
	}
	resp, err := http.Get(openLibraryUrl + "/authors/" + authorOl + "/works.json")
	if err != nil {
		return []Work{}, err
	}

	decoder := json.NewDecoder(resp.Body)

	entries := WorksResponse{}
	err = decoder.Decode(&entries)
	return entries.Entries, err
}

// @Summary      Save censored authors
// @Description  Save censored authors
// @Param        authors  body  []string  true  "[]string JSON"
// @Success      200
// @Router       /censors [post]
func enpSaveAuthorsToCensors(c *gin.Context) {
	authors := make([]string, 0)

	c.BindJSON(&authors)

	censoredDb = censoredDb.GetInstace(censoredDb)
	censoredDb.saveCensoredAuthors(authors)
	viper.Set("censors", censoredDb.getCensoredAuthors())
	viper.WriteConfig()

	c.Writer.WriteHeader(http.StatusOK)
}

type Config struct {
	Server      server
	OpenLibrary openLibrary
	Censors     []string `mapstructer:"censors" validate:"dive,censor"`
}

type server struct {
	Addr       string `mapstructer:"addr" validate:"hostname_port"`
	BaseApiUrl string `mapstructer:"baseApiUrl" validate:"required"`
	SwaggerUrl string `mapstructer:"swaggerUrl"`
	MetricsUrl string `mapstructer:"metricsUrl"`
}

type openLibrary struct {
	BaseUrl     string `mapstructer:"baseUrl" validate:"url"`
	ResultLimit uint16 `mapstructer:"resultLimit validate:"gte=0,lte=50""`
}

func CustomValidation(fl validator.FieldLevel) bool {
	value := fl.Field().String()
	return strings.HasPrefix(value, "OL")
}

func main() {
	viper.SetConfigFile("./config.json")
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal(err)
	}
	err = viper.Unmarshal(&config)
	if err != nil {
		log.Fatalf("unable to decode into struct, %v", err)
	}

	validate = *validator.New()
	err = validate.RegisterValidation("censor", CustomValidation)
	if err != nil {
		fmt.Println("Error registering custom validation :", err.Error())
		return
	}
	err = validate.Struct(&config)
	if err != nil {
		fmt.Println("validation error:", err)
		return
	}

	// @BasePath /api/v1
	apiUrl := config.Server.BaseApiUrl + "/v1"
	openLibraryUrl = config.OpenLibrary.BaseUrl
	censoredDb = censoredDb.GetInstace(censoredDb)
	censoredDb.saveCensoredAuthors(config.Censors)
	r := gin.Default()
	r.Use(handler())

	r.GET("/", getHello)
	r.GET(apiUrl+"/version", enpGetVersion)
	r.GET(apiUrl+"/authors", getAuthorsByBookId)
	r.GET(apiUrl+"/books", enpGetBooksByAuthor)
	r.POST(apiUrl+"/censors", enpSaveAuthorsToCensors)
	r.GET(config.Server.SwaggerUrl, ginSwagger.WrapHandler(swaggerFiles.Handler))
	r.GET(config.Server.MetricsUrl, gin.WrapH(promhttp.Handler()))

	// http.ListenAndServe(":2112", nil)

	server := http.Server{
		Addr:    config.Server.Addr,
		Handler: r,
	}

	go func() {
		// r.Run()
		err := server.ListenAndServe()
		if err != nil {
			log.Fatal(err)
		}
	}()

	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	<-ctx.Done()
	// fmt.Println(ctx.Err()) // prints "context canceled"
	stop() // stop receiving signal notifications as soon as possible.
	fmt.Println("shutting down gracefully, press Ctrl+C again to force")

	timeoutCtx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	if err := server.Shutdown(timeoutCtx); err != nil {
		fmt.Println(err)
	}
}
